#! /bin/bash

sudo apt update

sudo apt install -y openssh-server g++ build-essential software-properties-common curl git git-flow mysql-server mysql-client libmysqlclient-dev mesa-common-dev libglu1-mesa-dev libboost-all-dev libconfig++-dev libxerces-c-dev libzip-dev xterm xvnc4viewer vnc4server net-tools mc ncdu iptraf-ng htop gcc-multilib g++-multilib gcc-4.8-multilib g++-4.8-multilib vim qt5-default qttools5-dev-tools qt5-doc qtcreator libqt5sql5-mysql
